#!/usr/bin/env bash
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : do_update.sh                                                  ##
##  Project   : my_computer_tidy_and_clean                                    ##
##  Date      : Feb 24, 2020                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt 2020                                                  ##
##                                                                            ##
##  Description :                                                             ##
##                                                                            ##
##---------------------------------------------------------------------------~##

##----------------------------------------------------------------------------##
## Imports                                                                    ##
##----------------------------------------------------------------------------##
source /usr/local/src/stdmatt/shellscript_utils/main.sh


##----------------------------------------------------------------------------##
## Variables                                                                  ##
##----------------------------------------------------------------------------##
OS_NAME="$(pw_os_get_simple_name)";

SCRIPT_DIR="$(pw_get_script_dir)";
SRC_DIR="${SCRIPT_DIR}/..";
SRC_PLATFORM_DIR="${SRC_DIR}/${OS_NAME}";

USER_HOME="$(pw_find_real_user_home)";
PROJECTS_PATH="$USER_HOME/Documents/Projects/stdmatt";


##----------------------------------------------------------------------------##
## Functions                                                                  ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
call_install_script()
{
    PROJECT_DIR="${PROJECTS_PATH}/${1}";
    NEEDS_SUDO="$2";

    echo "Update tool ($PROJECT_DIR)";
    cd "$PROJECT_DIR";
        git pull;
        if [ -n "$NEEDS_SUDO" ]; then
            pw_as_super_user ./install.sh;
        else
            ./install.sh
        fi;
    cd -;
}



##----------------------------------------------------------------------------##
## Script                                                                     ##
##----------------------------------------------------------------------------##
##
## Easier to handle paths if everything is realative to the
## script's directory ;D
cd "$SCRIPT_DIR";
echo "Currently at directory: ($PWD)";

##
## Parse the command line args.
SHOULD_UPDATE_TOOLS=$(pw_getopt_exists "--tools" "$@");
SHOULD_UPDATE_SYSTEM=$(pw_getopt_exists "--system" "$@");
SHOULD_UPDATE_ALL=$(pw_getopt_exists "--all" "$@");

if [ -n "$SHOULD_UPDATE_ALL" ]; then
    SHOULD_UPDATE_TOOLS="--tools";
    SHOULD_UPDATE_SYSTEM="--system";
fi


if [ -n "$SHOULD_UPDATE_TOOLS" ]; then
    echo "User home is: ($USER_HOME)";
    echo "Projects path is: ($PROJECTS_PATH)";

    ## Libs.
    call_install_script libs/shellscript_utils "needs_sudo";

    ## Tools.
    call_install_script personal/dots
    call_install_script personal/git_utils
    call_install_script personal/project_creator  "needs_sudo";
    call_install_script personal/license_header   "needs_sudo";
    call_install_script personal/ichoices         "needs_sudo";
    call_install_script personal/bump_the_version "needs_sudo";

    call_install_script tools/gosh         "needs_sudo";
    call_install_script tools/manpdf       "needs_sudo";
    call_install_script tools/repochecker  "needs_sudo";
fi

##
## Update things that are platform dependent.
if [ -n "$SHOULD_UPDATE_SYSTEM" ]; then
    INSTALL_SCRIPT="${SRC_PLATFORM_DIR}/do_install.sh";
    UPDATE_SCRIPT="${SRC_PLATFORM_DIR}/do_update.sh";

    echo "Calling update for ($OS_NAME) at ($INSTALL_SCRIPT)";
    test -f "$INSTALL_SCRIPT" && "$INSTALL_SCRIPT" $@

    echo "Calling update for ($OS_NAME) at ($UPDATE_SCRIPT)";
    test -f "$UPDATE_SCRIPT" && "$UPDATE_SCRIPT" $@
fi;
