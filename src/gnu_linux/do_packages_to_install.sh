#!/usr/bin/env bash
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : do_packages_to_install.sh                                     ##
##  Project   : my_computer_tidy_and_clean                                    ##
##  Date      : Feb 24, 2020                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt 2020                                                  ##
##                                                                            ##
##  Description :                                                             ##
##                                                                            ##
##---------------------------------------------------------------------------~##

##----------------------------------------------------------------------------##
## Imports                                                                    ##
##----------------------------------------------------------------------------##
source /usr/local/src/stdmatt/shellscript_utils/main.sh


##----------------------------------------------------------------------------##
## Variables                                                                  ##
##----------------------------------------------------------------------------##
SCRIPT_DIR=$(pw_get_script_dir);


##----------------------------------------------------------------------------##
## Functions                                                                  ##
##----------------------------------------------------------------------------##
install_packages()
{
    ## @notice(stdmatt); We're assuming Debian based distros...
    local FILENAME="$1";
    while IFS= read -r LINE; do
        if [ -n "$(pw_string_starts_with "$LINE" "--")" ]; then
            continue;
        fi;
        if [ -z "$LINE" ]; then
            continue;
        fi;

        pw_FG "--> Installing $LINE <--"
        sudo apt-get install -y "$LINE";
    done < "$FILENAME";
}

##----------------------------------------------------------------------------##
## Script                                                                     ##
##----------------------------------------------------------------------------##
PACKAGE_GROUPS="";
## If no argumetns are given install everything under ./packages
if [ -z "$BASH_ARGC" ]; then
    PACKAGE_GROUPS=$(ls "${SCRIPT_DIR}/packages");
else
    PACKAGE_GROUPS="$@";
fi

## Remove the extensions.
PACKAGE_GROUPS=$(echo "$PACKAGE_GROUPS" | sed s/\.txt//g);
for PACKAGE_GROUP in $PACKAGE_GROUPS; do
    install_packages "${SCRIPT_DIR}/packages/${PACKAGE_GROUP}.txt"
done;

#  "$PROGRAMS";
# install_packages "$LIBS";
# install_packages "$DOCS";
